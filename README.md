# CoreVault

An encrypted vault app to store files for C Suite.
Based on CryFS.

Implementaion
-------------

* We have a single database file, encrypted suitably, which acts as an access to the Vault.
* All files and folders added to the vault will be stored in a dedicated folder.
* Each entry will be stored separately with a unique encryption key generated randomly by the Vault.
* Opening the vault (after entering the passcode) will display all the entries.
* If you want to open any file, then only that file or folder will be decrypted and shown to the user.
* This ensures that rest of the contents of the Vault are secure.
* Please remember that if your Vault PassCode is compromised, then the whole Vault is compromised.
* Please note the if you forget your Vault PassCode, the contents of the Vault cannot be recovered.

This implementation is not complete

### Download
You can download latest release.
* [Source](https://gitlab.com/cubocore/focalapps/corevault/tags)
* [ArchPackages](https://gitlab.com/cubocore/wiki/tree/master/ArchPackages)

### Dependencies:
* qt5-base
* [cryfs](https://www.cryfs.org/)
* [libcprime](https://gitlab.com/cubocore/libcprime)

### Information
Please see the Wiki page for this info.[Wiki page](https://gitlab.com/cubocore/wiki) .
* Changes in latest release
* Build from the source
* Tested In
* Known Bugs
* Help Us

### Feedback
* We need your feedback to improve the CoreApps.Send us your feedback through GitLab [issues](https://gitlab.com/groups/cubocore/-/issues).
