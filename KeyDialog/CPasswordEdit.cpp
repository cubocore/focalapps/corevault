/*
    *
    * This file is a part of CPrime ShareIt.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This file was originally written by Marcus Britanicus for NewBreeze.
    * It's been modified suitably for use with CoreApps
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include "CPasswordEdit.hpp"

QString stylesheet = QString(
	"QLabel {"
	"	border: 1px solid %1;"
	"	border-radius: 2px;"
	"	background-color: white;"
	"}"
	"QLabel:disabled {"
	"	background-color: transparent;"
	"}"
);

CPasswordEdit::CPasswordEdit( QWidget *parent ) : QLabel( parent ) {

	QLabel::setText( "<font color='darkgray'>Click to set the password</font>" );

	setAlignment( Qt::AlignCenter );
	setFixedHeight( 32 );

	setStyleSheet( stylesheet.arg( "lightgray") );

	setMouseTracking( true );
};

QString CPasswordEdit::text() {

	return mText;
};

void CPasswordEdit::setText( QString txt ) {

	if ( not txt.count() ) {

		mText.clear();

		QLabel::setText( "<font color='darkgray'>Click to set the password</font>" );
		return;
	}

	mText = txt;

	QLabel::setText( QString::fromUtf8( "\u2B22\u2B22\u2B22\u2B22\u2B22\u2B22\u2B22\u2B22\u2B22\u2B22" ) );
	emit passwordSet();
};

void CPasswordEdit::setOkay() {

	setDisabled( true );
	QLabel::setText( QString::fromUtf8( "<font color='darkgreen'><b>Password Validated ✓</b></font>" ) );
};

void CPasswordEdit::clear() {

	setEnabled( true );
	QLabel::setText( "<font color='darkgray'>Click to set the password</font>" );
};

void CPasswordEdit::mousePressEvent( QMouseEvent *mEvent ) {

	mEvent->accept();
	setStyleSheet( stylesheet.arg( "#7EB6FF" ) );
};

void CPasswordEdit::mouseReleaseEvent( QMouseEvent *mEvent ) {

	if ( not rect().contains( mEvent->pos() ) ) {
		mEvent->accept();
		return;
	}

	CPasswordInput *pInput = new CPasswordInput( qobject_cast<QWidget*>( parent() ) );
	if ( pInput->exec() == QDialog::Accepted ) {

		setText( pInput->password() );
		pInput->clear();
	}

	setStyleSheet( stylesheet.arg( "lightgray") );
	mEvent->accept();
};
