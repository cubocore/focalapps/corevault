/*
    *
    * This file is a part of CPrime ShareIt.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This file was originally written by Marcus Britanicus for NewBreeze.
    * It's been modified suitably for use with CoreApps
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include "CPasswordInput.hpp"

CPasswordInput::CPasswordInput( QWidget *parent ) : QDialog( parent ) {

	mOkay = false;

	createGUI();
	setWindowProperties();
};

QString CPasswordInput::password() {

	return mPassword;
};

void CPasswordInput::clear() {

	pKeyPad->clear();
	passwdLE->clear();
	mPassword.clear();
};

void CPasswordInput::createGUI() {

	pKeyPad = new CPatternPad( this );
	connect( pKeyPad, SIGNAL( inputComplete() ), this, SLOT( setPassword() ) );
	connect( pKeyPad, SIGNAL( inputComplete() ), this, SLOT( accept() ) );

	passwdLE = new QLineEdit();

	passwdLE->setAlignment( Qt::AlignCenter );
	passwdLE->setEchoMode( QLineEdit::Password );
	passwdLE->setStyleSheet( "lineedit-password-character: 8729;" );
	passwdLE->setFixedHeight( 32 );

	connect( passwdLE, SIGNAL( returnPressed() ), this, SLOT( setPassword() ) );
	connect( passwdLE, SIGNAL( returnPressed() ), this, SLOT( accept() ) );

	QVBoxLayout *base = new QVBoxLayout();
	base->addWidget( new QLabel( "Draw your password pattern below:", this ) );
	base->addWidget( pKeyPad );
	base->addWidget( new QLabel( "Or type in a password:", this ) );
	base->addWidget( passwdLE );

	setLayout( base );
};

void CPasswordInput::setWindowProperties() {

    setWindowTitle( "CoreVault - Input Password" );
	setWindowIcon( QIcon( ":/icons/CoreApps.png" ) );

    this->resize( 360, 480 );

    if ( sm->value( "CoreApps", "TouchMode" ) ) {
        if ( not sm->value( "CoreApps", "TabletMode" ) ){
            setFixedSize( qApp->primaryScreen()->size() * .8 );
        }
    }
};

void CPasswordInput::setPassword() {

	if ( pKeyPad == qobject_cast<CPatternPad*>( sender() ) ) {
		mPassword = pKeyPad->password();
		pKeyPad->clear();
	}

	else if ( ( passwdLE == qobject_cast<QLineEdit*>( sender() ) ) ) {
		mPassword = passwdLE->text();
		passwdLE->clear();
	}
};
