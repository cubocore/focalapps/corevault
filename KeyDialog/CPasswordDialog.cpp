/*
    *
    * This file is a part of CPrime ShareIt.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This file was originally written by Marcus Britanicus for NewBreeze.
    * It's been modified suitably for use with CoreApps
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include "CPasswordDialog.hpp"

CPasswordDialog::CPasswordDialog( QWidget *parent ) : QDialog( parent ) {

	createGUI();
	setWindowProperties();
};

QByteArray CPasswordDialog::password() {

	return passwd1LE->text().toLocal8Bit();
};

void CPasswordDialog::clear() {

	passwd1LE->clear();
	passwd2LE->clear();
};

void CPasswordDialog::createGUI() {

	QLabel *passwd1Lbl = new QLabel( "Input the &password:", this );
	passwd1LE = new CPasswordEdit( this );
	passwd1Lbl->setBuddy( passwd1LE );

	QLabel *passwd2Lbl = new QLabel( "Input the same password, &again:", this );
	passwd2LE = new CPasswordEdit( this );
	passwd2Lbl->setBuddy( passwd2LE );

	connect( passwd2LE, SIGNAL( passwordSet() ), this, SLOT( validatePassword() ) );

    QLabel *warnings = new QLabel( "Please set the vault password and remember it. If the vault password is forgotten, "
                                   "you <b>cannot</b> retrieve the encrypted data from files/folders. You may set a pattern or use a text password.", this );
    warnings->setWordWrap(true);

	okBtn = new QPushButton( QIcon::fromTheme( "dialog-ok-apply" ), "Okay" );
	connect( okBtn, SIGNAL( clicked() ), this, SLOT( accept() ) );
	okBtn->setDisabled( true );

	QPushButton *cancelBtn = new QPushButton( QIcon::fromTheme( "dialog-cancel" ), "&Cancel" );
	connect( cancelBtn, SIGNAL( clicked() ), this, SLOT( reject() ) );

	QHBoxLayout *btnLyt = new QHBoxLayout();
	btnLyt->addStretch();
	btnLyt->addWidget( okBtn );
	btnLyt->addWidget( cancelBtn );

	QVBoxLayout *baseLyt = new QVBoxLayout();

	baseLyt->addWidget( passwd1Lbl );
	baseLyt->addWidget( passwd1LE );
	baseLyt->addWidget( passwd2Lbl );
	baseLyt->addWidget( passwd2LE );
    baseLyt->addWidget(warnings);
	baseLyt->addStretch();
	baseLyt->addLayout( btnLyt );

	setLayout( baseLyt );
};

void CPasswordDialog::setWindowProperties() {

	setWindowTitle( "CoreApps - Input Password" );
	setWindowIcon( QIcon( ":/icons/appearance.png" ) );

//	QRect scrnSize = QDesktopWidget().screenGeometry();
//	int hpos = ( int )( ( scrnSize.width() - 600 ) / 2 );
//	int vpos = ( int )( ( scrnSize.height() - 200 ) / 2 );
//	move( hpos, vpos );

//	setFixedSize( 600, 180 );
    // set window size
    this->resize( 360, 480 );

    if ( sm->value( "CoreApps", "TouchMode" ) ) {
        if ( not sm->value( "CoreApps", "TabletMode" ) ){
            setFixedSize( qApp->primaryScreen()->size() * .8 );
        }
    }
};

void CPasswordDialog::validatePassword() {

	if ( passwd2LE->text() == passwd1LE->text() ) {

		okBtn->setEnabled( true );
		return;
	}

	/* When inside the while loop, we do not need CPasswordEdit::passwordSet() signal. */
	disconnect( passwd2LE, SIGNAL( passwordSet() ), this, SLOT( validatePassword() ) );

	do {
		int reply = QMessageBox::critical(
			this,
			"CoreApps - Password Mismatch",
			"The two passwords you have input do not match. Please input the same password in both the password input boxes.",
			QMessageBox::Cancel, QMessageBox::Ok
		);

		if ( reply == QMessageBox::Cancel ) {

			passwd1LE->setText( QString() );
			passwd2LE->setText( QString() );

			connect( passwd2LE, SIGNAL( passwordSet() ), this, SLOT( validatePassword() ) );

			return;
		}

		CPasswordInput *pInput = new CPasswordInput( this );
		if ( pInput->exec() == QDialog::Accepted ) {

			passwd2LE->setText( pInput->password() );
			pInput->clear();
		}
	} while ( passwd2LE->text() != passwd1LE->text() );

	okBtn->setEnabled( passwd2LE->text() == passwd1LE->text() );

	/* Now that we are outside the while loop, we renable the signal */
	connect( passwd2LE, SIGNAL( passwordSet() ), this, SLOT( validatePassword() ) );
};
