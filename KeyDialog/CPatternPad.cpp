/*
    *
    * This file is a part of CPrime ShareIt.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This file was originally written by Marcus Britanicus for NewBreeze.
    * It's been modified suitably for use with CoreApps
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include "CPatternPad.hpp"

inline static quint64 qHash( const QPoint key ) {

	return qHash( static_cast<qint64>( key.x() ) << 32 | key.y() );
};

CPatternPad::CPatternPad( QWidget *parent ) : QWidget( parent ) {

	mInvalid = false;
	pressed = false;
	curPos = QPoint();

    setWindowTitle( "CoreVault - Input the Password" );

    setFixedSize( 300, 300 );

	centers.clear();
	for( int y = 30; y <= 270; y += 60 )
		for( int x = 30; x <= 270; x += 60 )
			centers << QPoint( x, y );

	Q_FOREACH( QPoint center, centers )
		circles << Circle( center, 25 );

	for( int i = 0; i < 25; i++ )
		pointsCharMap[ centers.at( i ) ] = QChar( i + 65 );
};

QString CPatternPad::password() {

    return QCryptographicHash::hash( m_password.toUtf8(), QCryptographicHash::Sha3_512 ).toHex();

};

void CPatternPad::clear() {

	mInvalid = false;
	points.clear();
	m_password.clear();

	repaint();
};

void CPatternPad::setInvalid() {

	mInvalid = true;
	repaint();
};

void CPatternPad::mousePressEvent( QMouseEvent *mEvent ) {

	if ( mEvent->button() != Qt::LeftButton )  {
		mEvent->ignore();
		return;
	}

	pressed = true;
	mInvalid = false;

	points.clear();
	m_password.clear();

	grabMouse();

	Q_FOREACH( Circle circle, circles ) {
		if ( circle.contains( mEvent->pos() ) ) {
			points << circle.center();
			m_password += pointsCharMap.value( circle.center() );
			break;
		}

		else  {
			continue;
		}
	}

	repaint();

	mEvent->accept();
};

void CPatternPad::mouseMoveEvent( QMouseEvent *mEvent )  {

	if ( pressed )  {
		curPos = mEvent->pos();
		Q_FOREACH( Circle circle, circles ) {
			if ( circle.contains( curPos ) ) {
				/* Check if this point is the previous point */
				if ( points.last() != circle.center() ) {
					points << circle.center();
					m_password += pointsCharMap.value( circle.center() );
					break;
				}

				else  {
					continue;
				}
			}

			else  {
				continue;
			}
		}
	}

	else {
		QWidget::mouseMoveEvent( mEvent );
	}

	repaint();

	mEvent->accept();
};

void CPatternPad::mouseReleaseEvent( QMouseEvent *mEvent )  {

	pressed = false;
	curPos = QPoint();

	releaseMouse();
	repaint();

	/* We close it as soon as we input the password */
	inputComplete();

	mEvent->accept();
};

void CPatternPad::paintEvent( QPaintEvent *pEvent )  {

	QPainter *painter = new QPainter( this );

	painter->setRenderHint( QPainter::Antialiasing );
	painter->setPen( QColor( 0x00, 0x66, 0xFF ) );

	/* Draw dots */
	Q_FOREACH( QPoint center, centers )  {
		painter->drawEllipse( center, 1, 1 );
		painter->drawEllipse( center, 2, 2 );
		painter->drawEllipse( center, 3, 3 );
	}

	if ( points.count() > 1 ) {
		// Draw selected points
		if ( mInvalid )
			painter->setPen( QPen( QColor( 177,  79, 79 ), 1.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );

		else
			painter->setPen( QPen( QColor( 177,  79, 154 ), 1.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );

		// Paint only the last three points
		painter->drawEllipse( points.at( points.count() - 1 ), 20, 20 );
		painter->drawEllipse( points.at( points.count() - 2 ), 20, 20 );
		if ( points.count() >= 3 )
			painter->drawEllipse( points.at( points.count() - 3 ), 20, 20 );

		// Draw lines
		if ( mInvalid )
			painter->setPen( QPen( QColor( 177,  79, 79 ), 3.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );

		else
			painter->setPen( QPen( QColor( 177,  79, 154 ), 3.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );

		painter->drawLine( points.at( points.count() - 2 ), points.at( points.count() - 1 ) );
		if ( points.count() >= 3 )
			painter->drawLine( points.at( points.count() - 3 ), points.at( points.count() - 2 ) );
	}

	else {
		if ( mInvalid )
			painter->setPen( QPen( QColor( 177,  79, 79 ), 1.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );

		else
			painter->setPen( QPen( QColor( 177,  79, 154 ), 1.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );

		if( pressed and points.count() )
			painter->drawEllipse( points.last(), 20, 20 );
	}

	if ( not curPos.isNull() )
		painter->drawLine( points.last(), curPos );

	painter->end();

	pEvent->accept();
};
