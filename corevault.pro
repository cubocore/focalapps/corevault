#-------------------------------------------------
#
# Project created by QtCreator 2020-06-15T09:12:54
#
#-------------------------------------------------

QT += widgets

TARGET   = corevault
TEMPLATE = app

VERSION = 4.0.0

# Library section
unix:!macx: LIBS += -lcprime

# thread - Enable threading support
# silent - Do not print the compilation syntax
CONFIG  += thread silent

# Disable QDebug on Release build
CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

# Warn about deprecated features
DEFINES += QT_DEPRECATED_WARNINGS
# Disable all deprecated features before Qt 5.15
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x051500

# Build location
BUILD_PREFIX = $$(CA_BUILD_DIR)
isEmpty( BUILD_PREFIX ) {
        BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/vault/moc-plugins/
OBJECTS_DIR   = $$BUILD_PREFIX/vault/obj-plugins/
RCC_DIR       = $$BUILD_PREFIX/vault/qrc-plugins/
UI_DIR        = $$BUILD_PREFIX/vault/uic-plugins/

# C++17 Support for Qt5
QMAKE_CXXFLAGS += -std=c++17

DEFINES += STANDALONE

unix {
	isEmpty(PREFIX) {
		PREFIX = /usr
	}
	BINDIR = $$PREFIX/bin

	INSTALLS += target
	target.path = $$BINDIR

	QMAKE_RPATHDIR += $$PREFIX/lib/CoreApps/
}

INCLUDEPATH += . CryFS KeyDialog Salsa20

HEADERS += \
    CryFS/CryFS.hpp \
    Circle.hpp \
    KeyDialog/CPasswordDialog.hpp \
    KeyDialog/CPasswordEdit.hpp \
    KeyDialog/CPasswordInput.hpp \
    KeyDialog/CPatternPad.hpp \
    Salsa20/CSalsa20.hpp \
    Salsa20/Salsa20.hpp \
    Vault.hpp \
    VaultDatabase.hpp \
#	VaultGui.hpp \
    corevault.h
#    VaultPlugin.hpp

SOURCES += \
    Circle.cpp \
    CryFS/CryFS.cpp \
    KeyDialog/CPasswordDialog.cpp \
    KeyDialog/CPasswordEdit.cpp \
    KeyDialog/CPasswordInput.cpp \
    KeyDialog/CPatternPad.cpp \
    Main.cpp \
    Salsa20/CSalsa20.cpp \
    Vault.cpp \
    VaultDatabase.cpp \
#	VaultGui.cpp \
    corevault.cpp
	#VaultPlugin.cpp

FORMS += \
    corevault.ui


