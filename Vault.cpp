/*
    *
    * This file is a part of CPrime ShareIt.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This file was originally written by Marcus Britanicus for CoreApps.
    * It's been modified suitably for use with CoreApps
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include "Vault.hpp"
#include "CPasswordDialog.hpp"
#include "CPasswordInput.hpp"
#include "CryFS/CryFS.hpp"
#include "Salsa20/CSalsa20.hpp"

#include <cprime/filefunc.h>

QByteArray Vault::vaultPass = QByteArray();
Vault::Options Vault::mKeyStoreOption = Vault::AskVaultKeyEverytime;

Vault *Vault::vault = NULL;

Vault::Vault() {

	init = true;
	mKeyStoreOption = Vault::AskVaultKeyEverytime;
};

Vault* Vault::instance() {

	if ( not vault or vault->init )
		vault = new Vault();

	return vault;
};

void Vault::setVaultOption( Vault::Options opt ) {

	mKeyStoreOption = opt;
};

bool Vault::decryptFile( QString path ) {

	QByteArray pass = vaultPassword();
	if ( not pass.count() )
		return false;

	if ( path.endsWith( ".s20" ) )
		path.chop( 4 );

	VaultRecord *record = VaultDatabase::recordForPath( path, pass );
	if ( not record->recordPass.size() )
		return false;

	if ( not CPrime::FileUtils::exists( path + ".s20" ) )
		return false;

	VaultDatabase::removeRecordForPath( path );
	if ( not record->recordPass.size() )
		return false;

	CSalsa20 s20( record->encPath );
	s20.decrypt( record->recordPass );

	return true;
};

bool Vault::encryptFile( QString path ) {

	QByteArray pass = vaultPassword();
	if ( not pass.count() )
		return false;

	if ( not CPrime::FileUtils::isFile( path ) )
		return false;

	VaultRecord *record = new VaultRecord();
	record->encPath = path + ".s20";
	record->type = QString( "file" );
	record->recordPass = generatePassword();

	CSalsa20 s20( path );
	s20.encrypt( record->recordPass );

	VaultDatabase::addRecordForPath( path, record, pass );

	return true;
};

bool Vault::decryptDirectory( QString path ) {

	QByteArray pass = vaultPassword();
	if ( not pass.count() )
		return false;

	VaultRecord *record = VaultDatabase::recordForPath( path, pass );
	if ( not record->recordPass.size() )
		return false;

	CryFS cryFS( record->encPath, path );
	return cryFS.mountDir( QString::fromLocal8Bit( record->recordPass ) );
};

bool Vault::encryptDirectory( QString path ) {

	if ( VaultDatabase::isEncryptedLocation( path ) ) {
		CryFS cryFS( QString( " " ), path );
		return cryFS.unmountDir();
	}

	else {
		Vault *vlt = Vault::instance();
		QByteArray pass = vlt->vaultPassword();
		if ( not pass.count() )
			return false;

		QString mtPt = CPrime::FileUtils::baseName( path );

		VaultRecord *record = new VaultRecord();
		record->encPath = CPrime::FileUtils::dirName( path ) + "." + mtPt;
		record->type = QString( "dir" );
		record->recordPass = vlt->generatePassword();

		CryFS cryFS( record->encPath, path );
		if ( cryFS.createCryFS( record->recordPass ) )
			return VaultDatabase::addRecordForPath( path, record, pass );

		else
			return false;
	}
};

bool Vault::isDirectoryDecrypted( QString path ) {

	if ( path.endsWith( "/" ) )
		path.chop( 1 );

	QProcess proc;
	proc.start( "df -h", QStringList() );
	proc.waitForFinished();

	QString output = QString::fromLocal8Bit( proc.readAllStandardOutput() );

	if ( output.contains( " " + path + "\n" ) )
		return true;

	return false;
};

QByteArray Vault::vaultPassword() {

	if ( not vaultDB.value( "Password" ).toByteArray().size() ) {
		QMessageBox *msgBox = new QMessageBox( NULL );
		msgBox->setWindowTitle( "CoreApps - Vault" );
		msgBox->setIcon( QMessageBox::Information );
		msgBox->setText( "Vault First Use" );
		msgBox->setInformativeText(
			"<p>It seems that this is the first time you are using CoreApps Vault. Please read the following disclaimer carefully.</p>"
			"<p><i>This implementation of Vault is given to you as is without any without any warranty under the terms of the "
			"GNU General Public License (version 3 or later). This implementation is <b>NOT CERTIFIED</b> by any cryptography expert. "
			"Vault is meant for keeping your private/sensitive stuff from prying eyes and not for secure storage of important data.</i></p>"
			"<p>Please accept that you are using Vault at your own risk and the developer is not responsible for any data loss.</p>"
		);
		msgBox->addButton( "&Accept", QMessageBox::AcceptRole );
		msgBox->addButton( "&Reject", QMessageBox::RejectRole );

		if ( msgBox->exec() == QMessageBox::RejectRole )
			return QByteArray();

//		QMessageBox::information( NULL, "CoreApps - Vault", "Please set the vault password and remember it. If the vault password is forgotten, "
//		"you <b>cannot</b> retrieve the encrypted data from files/folders. You may set a pattern or use a text password." );

		CPasswordDialog *pDlg = new CPasswordDialog( NULL );
		if ( pDlg->exec() )
			vaultDB.setValue( "Password", QCryptographicHash::hash( pDlg->password(), QCryptographicHash::Sha3_512 ) );
	}

	if ( ( mKeyStoreOption == Vault::StoreVaultKeyForSession ) and ( vaultPass.count() ) )
		return vaultPass;

	CPasswordInput *pIn = new CPasswordInput( NULL );
	pIn->exec();

	QByteArray passwd = pIn->password().toLocal8Bit();
	pIn->clear();

	if ( not VaultDatabase::checkVaultPassword( passwd ) ) {
		QMessageBox::information( NULL, "Vault Error", "You have entered the wrong password!" );
		return QByteArray();
	}

	else {
		if ( mKeyStoreOption == Vault::StoreVaultKeyForSession )
			vaultPass = passwd;

		return passwd;
	}
};

bool Vault::changeVaultPassword() {

	QByteArray oldPass = vaultPassword();
	QByteArray newPass;

	CPasswordDialog *pDlg = new CPasswordDialog( NULL );
	if ( pDlg->exec() ) {
		newPass = pDlg->password();
		vaultDB.setValue( "Password", QCryptographicHash::hash( pDlg->password(), QCryptographicHash::Sha3_512 ) );

		return VaultDatabase::changeVaultPassword( oldPass, newPass );
	}

	return false;
};

QByteArray Vault::generatePassword() {

	FILE *randF = fopen( "/dev/urandom", "r" );
	char randData[ 1025 ] = { 0 };
	fread( randData, 1, 1024, randF );
	fclose( randF );

	return QCryptographicHash::hash( QByteArray( randData, 1024 ), QCryptographicHash::Sha3_512 ).toHex();
};
