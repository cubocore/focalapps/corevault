/*
    *
    * This file is a part of CPrime ShareIt.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This file was originally written by Marcus Britanicus for NewBreeze.
    * It's been modified suitably for use with CoreApps
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QtCore>
#include "./Salsa20.hpp"

class CSalsa20 : public QObject {
	Q_OBJECT

	public:
		CSalsa20( QString path = QString(), QWidget *parent = 0 );

		void encrypt( QByteArray password );
		void decrypt( QByteArray password );

		QByteArray encryptData( QByteArray data, QByteArray password );
		QByteArray decryptData( QByteArray data, QByteArray password );

	private:
		void makeKeyFromString( QByteArray, uint8_t* );
		void makeIVFromString( QByteArray, uint8_t* );

		QWidget *mParent;
		QString mFilePath;
};
