/*
    *
    * This file is a part of CPrime ShareIt.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This file was originally written by Marcus Britanicus for NewBreeze.
    * It's been modified suitably for use with CoreApps
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include "CSalsa20.hpp"
#include <iostream>

CSalsa20::CSalsa20( QString path, QWidget *parent ) : QObject() {

	/* Parent for PasswordInput dialog */
	mParent = parent;

	/* Full path */
	mFilePath = path;
};

void CSalsa20::makeIVFromString( QByteArray text, uint8_t *iv ) {

	/* Get the hashed password to be used */
	QByteArray password = QCryptographicHash::hash( text, QCryptographicHash::Md5 );

	for( int i = 0; i < password.size(); i += 4 ) {
		for( int c = 0; c < 4; c++ ) {
			uint8_t value = 0;
			char ch = password.at( c );

			if( ch >= '0' && ch <= '9' )
				value = ch - '0';

			else if( ch >= 'A' && ch <= 'F' )
				value = ch - 'A' + 0x0A;

			else if ( ch >= 'a' && ch <= 'f' )
				value = ch - 'a' + 0x0A;

			iv[ i / 4 ] |= ( value << ( 4 - i * 4 ) );
		}
	}
};

void CSalsa20::makeKeyFromString( QByteArray text, uint8_t *key ) {

	/* Get the hashed password to be used */
	QByteArray password = QCryptographicHash::hash( text, QCryptographicHash::Md5 );

	for( int i = 0; i < password.size(); i += 2 ) {
		for( int c = 0; c < 2; c++ ) {
			uint8_t value = 0;
			char ch = password.at( c );

			if( ch >= '0' && ch <= '9' )
				value = ch - '0';

			else if( ch >= 'A' && ch <= 'F' )
				value = ch - 'A' + 0x0A;

			else if ( ch >= 'a' && ch <= 'f' )
				value = ch - 'a' + 0x0A;

			key[ i / 2 ] |= ( value << ( 4 - i * 4 ) );
		}
	}
};

void CSalsa20::encrypt( QByteArray password ) {

	Salsa20 s20;

	/* Initialization Vector: */
	uint8_t iv[ 8 ] = { 0 };
	makeIVFromString( password, iv );

	uint8_t passwd[ 32 ] = { 0 };
	makeKeyFromString( password, passwd );

	s20.setKey( passwd );
	s20.setIv( iv );

	QFile ifile( mFilePath );
	ifile.open( QFile::ReadOnly );

	// if ( exists( mFilePath + ".s20" ) ) {
		// QString on = mFilePath + ".s20";
		// if ( rename( on.toLocal8Bit().data(), ( on  + ".orig" ).toLocal8Bit().data() ) != 0 ) {
			// QMessageBox::information(
				// mParent,
				// "NewBreeze - File Encryption Failed",
				// "I am unable to rename the already existing encrypted file. This may be because you do not have write permission in this directory. "
				// "Please gain sufficient permissions and try again."
			// );

			// return;
		// }
	// }

	QFile ofile( mFilePath + ".s20" );
	ofile.open( QFile::WriteOnly );

	uint8_t *block;
	while ( not ifile.atEnd() ) {
		QByteArray ba = ifile.read( 8192 );
		block = reinterpret_cast<uint8_t*>( ba.data() );

		/* If 8192 bytes were read, then process it as a block */
		if ( ba.length() == 8192 ) {

			s20.processBlocks( block, block, 128 );
			ofile.write( reinterpret_cast<const char*>( block ), 8192 );
		}

		/* Otherwise process it as bytes */
		else {

			s20.processBytes( block, block, ba.length() );
			ofile.write( reinterpret_cast<const char*>( block ), ba.length() );
		}
	}

	ifile.close();
	ofile.close();

	/* Remove the input file */
	ifile.remove();
};

void CSalsa20::decrypt( QByteArray password ) {

	Salsa20 s20;

	/* Initialization Vector: */
	uint8_t iv[ 8 ] = { 0 };
	makeIVFromString( password, iv );

	uint8_t passwd[ 32 ] = { 0 };
	makeKeyFromString( password, passwd );

	s20.setKey( passwd );
	s20.setIv( iv );

	QFile ifile( mFilePath );
	ifile.open( QFile::ReadOnly );

	/* Output filepath */
	QString oFilePath = mFilePath;
	oFilePath.chop( 4 );

	// if ( exists( oFilePath ) ) {
		// if ( rename( oFilePath.toLocal8Bit().data(), ( oFilePath  + ".orig" ).toLocal8Bit().data() ) != 0 ) {
			// QMessageBox::information(
				// mParent,
				// "NewBreeze - File Encryption Failed",
				// "I am unable to rename the already existing decrypted file. This may be because you do not have write permission in this directory. "
				// "Please gain sufficient permissions and try again."
			// );

			// return;
		// }
	// }

	QFile ofile( oFilePath );
	ofile.open( QFile::WriteOnly );

	uint8_t *block;
	while ( not ifile.atEnd() ) {
		QByteArray ba = ifile.read( 8192 );
		block = reinterpret_cast<uint8_t*>( ba.data() );

		/* If 8192 bytes were read, then process it as a block */
		if ( ba.length() == 8192 ) {

			s20.processBlocks( block, block, 128 );
			ofile.write( reinterpret_cast<const char*>( block ), 8192 );
		}

		/* Otherwise process it as bytes */
		else {

			s20.processBytes( block, block, ba.length() );
			ofile.write( reinterpret_cast<const char*>( block ), ba.length() );
		}
	}

	ifile.close();
	ofile.close();

	/* Remove the input file */
	ifile.remove();
};

QByteArray CSalsa20::encryptData( QByteArray data, QByteArray password ) {

	Salsa20 s20;

	/* Initialization Vector: */
	uint8_t iv[ 8 ] = { 0 };
	makeIVFromString( password, iv );

	uint8_t passwd[ 32 ] = { 0 };
	makeKeyFromString( password, passwd );

	s20.setKey( passwd );
	s20.setIv( iv );

	uint8_t *block;
	block = reinterpret_cast<uint8_t*>( data.data() );

	s20.processBytes( block, block, data.length() );
	QByteArray ba = QByteArray( reinterpret_cast<const char*>( block ), data.length() );

	return ba;
};

QByteArray CSalsa20::decryptData( QByteArray data, QByteArray password ) {

	Salsa20 s20;

	/* Initialization Vector: */
	uint8_t iv[ 8 ] = { 0 };
	makeIVFromString( password, iv );

	uint8_t passwd[ 32 ] = { 0 };
	makeKeyFromString( password, passwd );

	s20.setKey( passwd );
	s20.setIv( iv );

	uint8_t *block;
	block = reinterpret_cast<uint8_t*>( data.data() );

	s20.processBytes( block, block, data.length() );
	QByteArray ba = QByteArray( reinterpret_cast<const char*>( block ), data.length() );

	return ba;
};
