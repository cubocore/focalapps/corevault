/*
    *
    * This file is a part of CPrime ShareIt.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This file was originally written by Marcus Britanicus for CoreApps.
    * It's been modified suitably for use with CoreApps
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include "Circle.hpp"

Circle::Circle( QPoint center, qint64 radius ) {

	m_center = center;
	m_radius = radius;
};

QPoint Circle::center() {

	return m_center;
};


qint64 Circle::radius() {

	return m_radius;
};

bool Circle::contains( QPoint point ) {

	qint64 d2 = pow( point.x() - m_center.x(), 2 ) + pow( point.y() - m_center.y(), 2 );

	return ( d2 <= m_radius * m_radius );
};

bool Circle::contains( int x, int y ) {

	return contains( QPoint( x, y ) );
};

bool Circle::intersects( Circle circle2 ) {

	QPoint c1 = m_center;
	QPoint c2 = circle2.center();

	qint64 dsqr = pow( c1.x() - c2.x(), 2 ) + pow( c1.y() - c2.y(), 2 );

	return dsqr <= pow( m_radius + circle2.radius(), 2 );
};
