/*
    *
    * This file is a part of CPrime ShareIt.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This file was originally written by Marcus Britanicus for CoreApps.
    * It's been modified suitably for use with CoreApps
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include "Global.hpp"

typedef struct _VaultRecord {
	QString encPath;
	QString type;
	QByteArray recordPass;
} VaultRecord;

static QSettings vaultDB( QDir( QStandardPaths::writableLocation( QStandardPaths::ConfigLocation ) ).filePath( "coreapps/Vault.db" ), QSettings::NativeFormat );

namespace VaultDatabase {

	bool isEncryptedLocation( QString path );

	/* VaultRecord object for a given path */
	VaultRecord* recordForPath( QString path, QByteArray vaultPass );

	/* Add a record to the database */
	bool addRecordForPath( QString path, VaultRecord *, QByteArray vaultPass );

	/* Remove a record to the database */
	bool removeRecordForPath( QString path );

	/* Check if the vault key is correct */
	bool checkVaultPassword( QByteArray vPass );

	/* Change password of the Vault */
	bool changeVaultPassword( QByteArray oldPass, QByteArray newPass );
};
