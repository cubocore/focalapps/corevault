#ifndef COREVAULT_H
#define COREVAULT_H

#include <QWidget>

#include "Global.hpp"
#include "Vault.hpp"
#include "VaultDatabase.hpp"

namespace Ui {
class corevault;
}

class VaultModel : public QStringListModel {
    Q_OBJECT

    public:
        enum EntryType {
            DIR = 0x59c35b,
            FILE
        };

        VaultModel() : QStringListModel() {

            setStringList( vaultDB.value( "Paths" ).toStringList() );

            QFileSystemWatcher *fsw = new QFileSystemWatcher();
            fsw->addPath( vaultDB.fileName() );
            connect(
                fsw, &QFileSystemWatcher::fileChanged, [=]() {
                    setStringList( vaultDB.value( "Paths" ).toStringList() );
                }
            );
        };

        QVariant data( const QModelIndex &index, int role = Qt::DisplayRole ) const override {

            if ( role == Qt::DecorationRole ) {
                QString data = QStringListModel::data( index, Qt::DisplayRole ).toString();
                if ( QFileInfo( data ).isDir() )
                    return QIcon::fromTheme( "folder-locked" );

                else
                    return QIcon::fromTheme( "application-pgp-encrypted" );
            }

            else if ( role == Qt::UserRole + 1 ) {
                QString data = QStringListModel::data( index, Qt::DisplayRole ).toString();
                if ( QFileInfo( data ).isDir() )
                    return EntryType::DIR;

                else
                    return EntryType::FILE;
            }

            return QStringListModel::data( index, role );
        };

        void refresh() {

            vaultDB.sync();
            setStringList( vaultDB.value( "Paths" ).toStringList() );
        }
};

class corevault : public QWidget
{
    Q_OBJECT

public:
    explicit corevault(QWidget *parent = nullptr);
    ~corevault();

    enum Options {
        StoreVaultKeyForSession = 0x39F064,					// Store the vault key in memory
        AskVaultKeyEverytime								// Ask the key from the user everytime; this is the default behaviour
    };

private slots:
    void on_openVault_clicked();

    void openSelectedFile(const QListWidgetItem &item);
private:
    Ui::corevault *ui;
    VaultModel *vaultModel;
    Vault *vault = Vault::instance();
    bool            touch;
    int             sideViewSize;
    QSize           sideViewIconSize, toolBarIconSize, listViewIconSize;

    void createActions();
    void startSetup();
    void loadSettings();
    void showSideView();
};

#endif // COREVAULT_H
