#include "corevault.h"
#include "ui_corevault.h"

#include "CPasswordDialog.hpp"
#include "CPasswordInput.hpp"
#include "CryFS/CryFS.hpp"
#include "Salsa20/CSalsa20.hpp"

#include <cprime/filefunc.h>
#include <cprime/themefunc.h>
#include <cprime/settingsmanage.h>
#include <cprime/appopenfunc.h>

corevault::corevault(QWidget *parent) :QWidget(parent),ui(new Ui::corevault)
{
    ui->setupUi(this);
    loadSettings();
    startSetup();
    createActions();

    /* First run */
    if ( not vaultDB.value( "Password" ).toByteArray().size() ) {
        QMessageBox *msgBox = new QMessageBox( NULL );
        msgBox->setWindowTitle( "CoreVault" );
        msgBox->setIcon( QMessageBox::Information );
        msgBox->setText( "Vault First Use" );
        msgBox->setInformativeText(
            "<p>It seems that this is the first time you are using CoreVault. Please read the following disclaimer carefully.</p>"
            "<p><i>This implementation of Vault is given to you as is without any without any warranty under the terms of the "
            "GNU General Public License (version 3 or later). This implementation is <b>NOT CERTIFIED</b> by any cryptography expert. "
            "Vault is meant for keeping your private/sensitive stuff from prying eyes and not for secure storage of important data.</i></p>"
            "<p>Please accept that you are using Vault at your own risk and the developer is not responsible for any data loss.</p>"
        );
        msgBox->addButton( "&Accept", QMessageBox::AcceptRole );
        msgBox->addButton( "&Reject", QMessageBox::RejectRole );

        if ( msgBox->exec() == QMessageBox::AcceptRole ) {
            CPasswordDialog *pDlg = new CPasswordDialog( NULL );
            if ( pDlg->exec() )
                vaultDB.setValue( "Password", QCryptographicHash::hash( pDlg->password(), QCryptographicHash::Sha3_512 ) );
        }
    }
}

corevault::~corevault()
{
    delete ui;
}
/**
 * @brief Setup ui elements
 */
void corevault::startSetup()
{
	vaultModel = new VaultModel();
	ui->vaultView->setModel(vaultModel);

    ui->page->setCurrentIndex(0);

    // set stylesheet
    ui->sideView->setStyleSheet(CPrime::ThemeFunc::getSideViewStyleSheet());
    ui->sideView->setFixedWidth(sideViewSize);

    // all toolbuttons icon size in sideView
    for (QToolButton *b : ui->sideView->findChildren<QToolButton *>()) {
        if (b) {
            b->setIconSize(sideViewIconSize);
        }
    }

    // all toolbuttons icon size in topBar
    for (QToolButton *b : ui->toolBar->findChildren<QToolButton *>()) {
        if (b) {
            b->setIconSize(toolBarIconSize);
        }
    }

    ui->vaultView->setIconSize(listViewIconSize);
//    ui->vaultView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);


    QScroller::grabGesture(ui->vaultView, QScroller::LeftMouseButtonGesture);

//    ui->menu->setVisible(0);
    ui->appTitle->setAttribute(Qt::WA_TransparentForMouseEvents);
    ui->appTitle->setFocusPolicy(Qt::NoFocus);
    this->resize(800, 500);

    if (touch) {
        this->setWindowState(Qt::WindowMaximized);

        if (not sm->value("CoreApps", "TabletMode")){
//            connect(ui->menu, &QToolButton::clicked, this, &corevault::showSideView);
            connect(ui->appTitle, &QToolButton::clicked, this, &corevault::showSideView);

            ui->sideView->setVisible(0);
//            ui->menu->setVisible(1);
        }
    }

    if (touch)
        connect(ui->vaultView, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(openSelectedFile(QListWidgetItem *)));
    else
        connect(ui->vaultView, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(openSelectedFile(QListWidgetItem *)));

}

/**
 * @brief Loads application settings
 */
void corevault::loadSettings()
{
    sideViewSize = sm->value("CoreApps", "SideViewSize");
    toolBarIconSize = sm->value("CoreApps", "ToolBarIconSize");
    listViewIconSize = sm->value("CoreApps", "ListViewIconSize");
    sideViewIconSize = sm->value("CoreApps", "SideViewIconSize");
    touch = sm->value("CoreApps", "TouchMode");
}


void corevault::showSideView()
{
    if (ui->sideView->isVisible())
        ui->sideView->setVisible(0);
    else
        ui->sideView->setVisible(1);
}

void corevault::createActions() {

    connect(
        ui->encryptB, &QPushButton::clicked, [=]() {
            QModelIndex cur = ui->vaultView->currentIndex();
            if ( cur.isValid() ) {
                QString path = cur.data().toString();
                int type = cur.data( Qt::UserRole + 1 ).toInt();

                if ( type == VaultModel::EntryType::DIR )
                    vault->encryptDirectory( path );

                else
                    vault->encryptFile( path );

                vaultModel->refresh();
            }
        }
    );

    connect(
        ui->decryptB, &QPushButton::clicked, [=]() {
            QModelIndex cur = ui->vaultView->currentIndex();
            if ( cur.isValid() ) {
                QString path = cur.data().toString();
                int type = cur.data( Qt::UserRole + 1 ).toInt();

                if ( type == VaultModel::EntryType::DIR )
                    vault->decryptDirectory( path );

                else
                    vault->decryptFile( path );

                vaultModel->refresh();
            }
        }
    );

    connect(
        ui->addFiles, &QPushButton::clicked, [=]() {
			QString path = QFileDialog::getOpenFileName( this, "CoreVault | Choose File", QDir::homePath() );
            if ( not path.isEmpty() )
                vault->encryptFile( path );

            vaultModel->refresh();
        }
    );

//	connect(
//		addDirBtn, &QPushButton::clicked, [=]() {
//			QString path = QFileDialog::getExistingDirectory( this, "Vault | Choose Directory", QDir::homePath() );
//			if ( not path.isEmpty() )
//				vault->encryptDirectory( path );

//			vaultModel->refresh();
//		}
//	);

    connect(
        // vaultView->selectionModel(), &QItemSelectionModel::currentChanged, [=]( const QModelIndex &cur, const QModelIndex & ) {
        ui->vaultView->selectionModel(), &QItemSelectionModel::currentChanged, [=]() {
            QModelIndex cur = ui->vaultView->currentIndex();
            if ( not cur.isValid() ) {
                ui->encryptB->setEnabled( false );
                ui->decryptB->setEnabled( false );
            }

            else {
                QString path = cur.data().toString();
                int type = cur.data( Qt::UserRole + 1 ).toInt();

                if ( type == VaultModel::EntryType::DIR ) {
                    if ( vault->isDirectoryDecrypted( path ) ) {
                        ui->encryptB->setEnabled( true );
                        ui->decryptB->setEnabled( false );
                    }

                    else {
                        ui->encryptB->setEnabled( false );
                        ui->decryptB->setEnabled( true );
                    }
                }

                /* We will never have an decrypted file: All files listed will be encrypted */
                else {
                    ui->encryptB->setEnabled( false );
                    ui->decryptB->setEnabled( true );
                }
            }
        }
    );
}

void corevault::on_openVault_clicked()
{
    // open the pass check dialog and if pass is valid then move to second page
	CPasswordDialog *pDlg = new CPasswordDialog( this );
	if ( pDlg->exec() ) {
		QString p1 = QCryptographicHash::hash( pDlg->password(), QCryptographicHash::Sha3_512 );
		QString p2 = vaultDB.value("Password").toString();

		if (p1 == p2) {
			ui->page->setCurrentIndex(1);
		} else {
			QMessageBox::critical(this, "Wrong password", "Please enter the correct password");
		}
	}
}

void corevault::openSelectedFile(const QListWidgetItem &item)
{

}
