/*
    *
    * Main.cpp - NewBreeze Vault StandAlone Executable
    *
*/

#include "Global.hpp"
#include "corevault.h"
#include <QDebug>

int main( int argc, char *argv[] ) {

	QApplication app( argc, argv );

    corevault *Gui = new corevault();
	Gui->show();

	return app.exec();
};
