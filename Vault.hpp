/*
    *
    * This file is a part of CPrime ShareIt.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This file was originally written by Marcus Britanicus for CoreApps.
    * It's been modified suitably for use with CoreApps
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include "Global.hpp"
#include "VaultDatabase.hpp"

class Vault {

	public:
		enum Options {
			StoreVaultKeyForSession = 0x39F064,					// Store the vault key in memory
			AskVaultKeyEverytime								// Ask the key from the user everytime; this is the default behaviour
		};

		static Vault* instance();

		static void setVaultOption( Vault::Options opt );

		/* Decrypt and view/edit file */
		bool decryptFile( QString path );

		/* Encrypt and store the file */
		bool encryptFile( QString path );

		/* Decrypt and mount the directory at @path */
		bool decryptDirectory( QString path );

		/* Unmount and encrypt the directory at @path */
		bool encryptDirectory( QString path );

		/* Check if the directory @path is mounted */
		bool isDirectoryDecrypted( QString path );

		/* Get vault pass */
		QByteArray vaultPassword();

		/* Change Vault Password */
		bool changeVaultPassword();

		/* Generate a random password */
		QByteArray generatePassword();

	private:
		Vault();

		static Vault* vault;
		bool init;

		static QByteArray vaultPass;
		static Vault::Options mKeyStoreOption;
};
