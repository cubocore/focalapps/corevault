/*
    *
    * This file is a part of CPrime ShareIt.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This file was originally written by Marcus Britanicus for CoreApps.
    * It's been modified suitably for use with CoreApps
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include "VaultDatabase.hpp"
#include "Salsa20/CSalsa20.hpp"

#include <cprime/filefunc.h>

bool VaultDatabase::isEncryptedLocation( QString path ) {

	/* Check if the path exists */
	QStringList paths = vaultDB.value( "Paths" ).toStringList();

	if ( CPrime::FileUtils::isFile( path ) and path.endsWith( ".s20" ) )
		path.chop( 4 );

	if ( paths.contains( path ) )
		return true;

	else {
		/* Typically, */
		QString cpath = CPrime::FileUtils::dirName( path ) + CPrime::FileUtils::baseName( path ).remove( 0, 1 );
	}

	return false;
};

VaultRecord* VaultDatabase::recordForPath( QString path, QByteArray vaultPass ) {

	/* Prepare the Vault Record Object */
	VaultRecord *rec = new VaultRecord();
	rec->encPath = QString();
	rec->type = QString();
	rec->recordPass = QByteArray();

	/* Check if the path exists */
	if ( not vaultDB.value( "Paths" ).toStringList().contains( path ) )
		return rec;

	/* Get the hash under which the data is stored */
	QString pathHash = QString( QCryptographicHash::hash( path.toLocal8Bit(), QCryptographicHash::Md5 ).toHex() );

	/* Get the data: @pathHash/Data */
	QByteArray data = vaultDB.value( pathHash + "/Data" ).toByteArray();

	/* Ready the Salsa20 decryptor */
	CSalsa20 s20;

	/* Decrypt the data */
	QByteArray info = s20.decryptData( data, vaultPass );
	QList<QByteArray> infoList = info.split( '\n' );

	/* Store the info in the Vault Record object */
	rec->encPath = QString::fromLocal8Bit( infoList.at( 0 ) );
	rec->type = QString::fromLocal8Bit( infoList.at( 1 ) );
	rec->recordPass = infoList.at( 2 );

	/* Return the Record */
	return rec;
};

bool VaultDatabase::addRecordForPath( QString path, VaultRecord *rec, QByteArray vaultPass ) {

	/* Check if the path exists */
	QStringList paths = vaultDB.value( "Paths" ).toStringList();
	if ( paths.contains( path ) )
		return ( recordForPath( path, vaultPass ) == rec );

	paths << path;

	/* Create data from @rec */
	QByteArray data;
	data += rec->encPath + "\n";
	data += rec->type + "\n";
	data += rec->recordPass + "\n";

	/* Encrypt the data */
	CSalsa20 s20;

	/* Overwrite the data by the encrypted bytes */
	data = s20.encryptData( data, vaultPass );

	/* Get the hash under which the data will be stored */
	QString pathHash = QString( QCryptographicHash::hash( path.toLocal8Bit(), QCryptographicHash::Md5 ).toHex() );

	/* Store the updated @paths and the encrypted record */
	vaultDB.setValue( "Paths", paths );
	vaultDB.setValue( pathHash + "/Data", data );

	vaultDB.sync();

	return true;
};

bool VaultDatabase::removeRecordForPath( QString path ) {

	/* Check if the path exists */
	QStringList paths = vaultDB.value( "Paths" ).toStringList();
	if ( not paths.contains( path ) )
		return true;

	/* Remove @path from the Paths list */
	paths.removeAll( path );

	/* Get the hash under which the data is stored */
	QString pathHash = QString( QCryptographicHash::hash( path.toLocal8Bit(), QCryptographicHash::Md5 ).toHex() );

	/* Store the update @paths */
	vaultDB.setValue( "Paths", paths );

	/* Remove the data under @pathHash */
	vaultDB.remove( pathHash );

	vaultDB.sync();

	return true;
};

bool VaultDatabase::checkVaultPassword( QByteArray vPass ) {

	/* Password hash */
	QByteArray vpHash = QCryptographicHash::hash( vPass, QCryptographicHash::Sha3_512 );

	return ( vaultDB.value( "Password" ).toByteArray() == vpHash );
};

bool VaultDatabase::changeVaultPassword( QByteArray oldPass, QByteArray newPass ) {

	CSalsa20 old20;
	CSalsa20 new20;

	Q_FOREACH( QString path, vaultDB.value( "Paths" ).toStringList() ) {
		QString pathHash = QString( QCryptographicHash::hash( path.toLocal8Bit(), QCryptographicHash::Md5 ).toHex() );
		QByteArray data = old20.decryptData( vaultDB.value( pathHash + "/data" ).toByteArray(), oldPass );
		data = new20.encryptData( data, newPass );

		vaultDB.setValue( pathHash + "/Data", data );

		vaultDB.sync();
	}

	return true;
};
